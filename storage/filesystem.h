#pragma once

// storage
#include "DLL.h"

// macros
#include "../macros/ClassMacros.h"

// functions
#include "../modules/linq/LinqSln/Linq/Linq.h"

// Qt
#include <QObject>
#include <QDir>
#include <QThread>
#include <QDateTime>

namespace Common
{
   namespace Storage
   {
      struct FileInfo
      {
         FileInfo(const QFileInfo& info)
            : path(info.absolutePath())
            , name(info.baseName())
            , extension(info.completeSuffix())
            , creationDate(info.created())
            , byteSize(info.size())
         {}

         QString 
            path,
            name,
            extension;

         QDateTime
            creationDate;

         std::size_t 
            byteSize;
      };

      class STORAGE_EXPORT FileSystem: public QObject
      {
         Q_OBJECT;
         MEMORY(FileSystem);

      public:

         FileSystem(QObject* parent = nullptr);
         ~FileSystem();

         void analyzeDrive(const QDir& dir);

      signals:

         void analyzePercentageChanged(double percentage);

         void fileAnalyzed(const QFileInfo& fileInfo);

         void dirAnalyzed(const QFileInfo& fileInfo);

         void percentageAnalyzed(double percentage);
         
      public slots:

         void startAnalysisInThread();

         void analyzeFolderInThread(QDir* dir);

      private /*MEMBERS*/:

         //QThread*
         //   m_analyzerThread;

         QDir
            m_startDir;

         std::size_t 
            m_totalSize = 0,
            m_analyzedSize = 0;

         int
            m_lastPercentage = 0;

         Common::Linq<FileInfo>
            m_foundDirs,
            m_foundFiles;

      };
   }
}