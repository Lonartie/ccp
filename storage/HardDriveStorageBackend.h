#pragma once
#include "StorageBackend.h"
#include "DLL.h"
#include "../macros/ClassMacros.h"
#include "../modules/linq/LinqSln/Linq/Linq.h"
#include <QByteArray>
#include <QString>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QFile>
#include <QBuffer>

namespace Common
{
   namespace Storage
   {
      class STORAGE_EXPORT HardDriveStorageBackend: public StorageBackend
      {
         MEMORY(HardDriveStorageBackend);

      public:
         HardDriveStorageBackend(const QString& location);

         virtual void Save(const QString& key, const QByteArray& data) override;
         virtual QByteArray Load(const QString& key) override;

         virtual ~HardDriveStorageBackend();

      private:

         void SaveToDisk();
         void LoadFromDisk();
         static void writeNode(const std::tuple<QString, QByteArray>& data, QXmlStreamWriter& writer);

         QString m_location;
         QFile m_file;

         Common::Linq<std::tuple<QString, QByteArray>> m_data;

         QXmlStreamReader m_reader;
         QXmlStreamWriter m_writer;

      };
   }
}
