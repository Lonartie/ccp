// storage
#include "filesystem.h"

// Qt
#include <QStorageInfo>

Common::Storage::FileSystem::FileSystem(QObject* parent)
   : QObject(parent)
   //, m_analyzerThread(new QThread(this))
{
   //_connect(m_analyzerThread, started, this, startAnalysisInThread);
}

Common::Storage::FileSystem::~FileSystem()
{}

void Common::Storage::FileSystem::analyzeDrive(const QDir& dir)
{
   QStorageInfo info(dir.absolutePath());
   m_totalSize = info.bytesTotal() - info.bytesFree();

   m_startDir = dir;
}

void Common::Storage::FileSystem::startAnalysisInThread()
{
   analyzeFolderInThread(new QDir(m_startDir));
}

void Common::Storage::FileSystem::analyzeFolderInThread(QDir* dir)
{
   auto _files = dir->entryInfoList(QDir::NoDotAndDotDot | QDir::Files);
   auto _dirs = dir->entryInfoList(QDir::NoDotAndDotDot | QDir::Dirs);

   delete dir;

   // analyze files in this directory
   for (auto& _file : _files)
   {
      m_foundFiles.Add(FileInfo(_file));
      emit fileAnalyzed(_file);

      m_analyzedSize += _file.size();
      auto new_percentage = m_analyzedSize / static_cast<double>(m_totalSize);
      
      if (static_cast<int>(new_percentage * 100) > m_lastPercentage)
      {
         m_lastPercentage = new_percentage * 100;
         emit percentageAnalyzed(new_percentage);
      }
   }

   // analyze dirs in this directory
   for (auto& _dir : _dirs)
   {
      m_foundDirs.Add(FileInfo(_dir));
      emit dirAnalyzed(_dir);
   }

   // analyze children dirs
   for (auto& _dir : _dirs)
   {
      analyzeFolderInThread(new QDir(_dir.absolutePath() + "/" + _dir.fileName()));
   }
}
