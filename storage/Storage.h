#pragma once

#include "StorageBackend.h"
#include "../macros/ClassMacros.h"
#include "../functions/TypeConversion.h"
#include <QByteArray>
#include <QString>

namespace Common
{
   // TODO: HardDriveStorage implementation
   // TODO: NetworkStorage implementation
   namespace Storage
   {
      template<typename BackendType>
      class Storage
      {
         MEMORY(Storage);

      public:
         template<typename ...Args>
         Storage(Args...args): backend(BackendType(args...)) {};
         Storage(Storage&& storage): backend(std::move(storage.backend)) {};
         Storage(const Storage& storage) = delete;

         template<typename T> void Save(const QString& key, const T& data);
         template<typename T> void Save(const QString& key, T* begin, T* end);

         template<typename T> T Load(const QString& key);

      private:

         BackendType backend;

      };

      /*
         #################### IMPLEMENTATION ####################
      */

      template<typename BackendType>
      template<typename T>
      T Common::Storage::Storage<BackendType>::Load(const QString& key)
      {
         if constexpr(std::is_same<T, QByteArray>::value)
         {
            return backend.Load(key);
         }
         else
         {
            return *Functions::FromQByteArray<T>(backend.Load(key));
         }
      }

      template<typename BackendType>
      template<typename T>
      void Common::Storage::Storage<BackendType>::Save(const QString& key, T* begin, T* end)
      {
         backend.Save(key, Functions::ToQByteArray(begin, end - begin));
      }

      template<typename BackendType>
      template<typename T>
      void Common::Storage::Storage<BackendType>::Save(const QString& key, const T& data)
      {
         if constexpr(std::is_same<T, QByteArray>::value)
         {
            backend.Save(key, data);
         }
         else
         {
            backend.Save(key, Functions::ToQByteArray(&data, 1));
         }
      }

   }
}
