#pragma once

#include "../macros/ClassMacros.h"
#include <QByteArray>
#include <QString>

namespace Common
{
   namespace Storage
   {
      class StorageBackend
      {
         MEMORY(StorageBackend);

      public:

         virtual void Save(const QString& key, const QByteArray& data) = 0;
         virtual QByteArray Load(const QString& key) = 0;
         virtual ~StorageBackend(){};
      };
   }
}