#include "MemoryStorageBackend.h"

Common::Storage::MemoryStorageBackend::MemoryStorageBackend()
{
   
}

void Common::Storage::MemoryStorageBackend::Save(const QString& key, const QByteArray& data)
{
   m_data.Add({key, data});
}

QByteArray Common::Storage::MemoryStorageBackend::Load(const QString& key)
{
   return m_data.
      Where(Lambda1(x, std::get<0>(x) == key)).
      Select(Lambda1(x, std::get<1>(x))).
      FirstOrDefault();
}

Common::Storage::MemoryStorageBackend::~MemoryStorageBackend()
{
   m_data.Clear();
}
