#pragma once

#include "Storage.h"
#include "../macros/ClassMacros.h"
#include "../functions/TypeConversion.h"
#include <QString>
#include <QByteArray>

namespace Common
{
   namespace Storage
   {
      template<typename T>
      class Serializable
      {
         MEMORY(Serializable);

      public:
         Serializable(T* child);

         template<typename BackendType>
         void Serialize(Storage<BackendType>& storage);

         template<typename BackendType>
         static T Deserialize(Storage<BackendType>& storage);

      private:

         T* m_this;
      };
      
      /*
         #################### IMPLEMENTATION ####################
      */

      template<typename T>
      Serializable<T>::Serializable(T* child)
         : m_this(child) {}

      template<typename T>
      template<typename BackendType>
      void Serializable<T>::Serialize(Storage<BackendType>& storage)
      {
         QString name = typeid(T).name();
         QByteArray name_raw = name.toLatin1();
         storage.Save("ClassName",
                      name_raw);

         storage.Save("ByteSize",
                      sizeof(T));

         storage.Save("DataRaw",
                      *m_this);
      }

      template<typename T>
      template<typename BackendType>
      T Serializable<T>::Deserialize(Storage<BackendType>& storage)
      {
         if (storage.Load<QByteArray>("ClassName") != typeid(T).name())
            return T{};

         if (storage.Load<std::size_t>("ByteSize") != sizeof(T))
            return T{};

         return storage.Load<T>("DataRaw");
      }

   }
}
