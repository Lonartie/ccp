#include "HardDriveStorageBackend.h"

#include <functional>
using namespace std::placeholders;

Common::Storage::HardDriveStorageBackend::HardDriveStorageBackend(const QString& location)
   : m_location(location)
   , m_file(location)
   , m_writer(&m_file)
   , m_reader(&m_file)
{}

Common::Storage::HardDriveStorageBackend::~HardDriveStorageBackend()
{}

void Common::Storage::HardDriveStorageBackend::Save(const QString& key, const QByteArray& data)
{
   m_data.Add({key, data});
   SaveToDisk();
}

QByteArray Common::Storage::HardDriveStorageBackend::Load(const QString& key)
{
   LoadFromDisk();

   return m_data.
      Where(Lambda1(x, std::get<0>(x) == key)).
      Select(Lambda1(x, std::get<1>(x))).
      FirstOrDefault();
}

void Common::Storage::HardDriveStorageBackend::SaveToDisk()
{
   m_file.setFileName(m_location);
   m_file.open(QIODevice::WriteOnly);

   m_writer.setAutoFormatting(true);
   m_writer.writeStartDocument("hdsb 0.1");
   m_writer.writeStartElement("root");
   m_data.Foreach(Lambda1(x, writeNode(x, m_writer)));
   m_writer.writeEndElement();
   m_writer.writeEndDocument();

   m_file.flush();
   m_file.close();
}

void Common::Storage::HardDriveStorageBackend::LoadFromDisk()
{
   m_file.setFileName(m_location);
   m_file.open(QIODevice::ReadOnly);
   m_data.Clear();

   while (m_reader.readNextStartElement())
   {
      if(m_reader.name() == "root" )
      {
         m_reader.readNextStartElement();
      }

      QString node_value = m_reader.readElementText();
      QString node_key = m_reader.name().toString();
      m_data.Add({node_value, node_key.toLatin1()});

      m_reader.readNext();
   }

   m_file.close();
}

void Common::Storage::HardDriveStorageBackend::writeNode(const std::tuple<QString, QByteArray>& data, QXmlStreamWriter& writer)
{
   writer.writeTextElement(std::get<0>(data), QString(std::get<1>(data)));
}

