#pragma once
#include "StorageBackend.h"
#include "DLL.h"
#include "../macros/ClassMacros.h"
#include "../modules/linq/LinqSln/Linq/Linq.h"
#include <QByteArray>
#include <QString>
#include <map>

namespace Common
{
   namespace Storage
   {
      class STORAGE_EXPORT MemoryStorageBackend : public StorageBackend
      {
         MEMORY(MemoryStorageBackend);
      public:

         MemoryStorageBackend();

         
         virtual void Save(const QString& key, const QByteArray& data) override;
         virtual QByteArray Load(const QString& key) override;

         virtual ~MemoryStorageBackend();

      private:

         Common::Linq<std::tuple<QString, QByteArray>> m_data;

      };
   }
}
