#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(STORAGE_LIB)
#  define STORAGE_EXPORT Q_DECL_EXPORT
# else
#  define STORAGE_EXPORT Q_DECL_IMPORT
# endif
#else
# define STORAGE_EXPORT
#endif