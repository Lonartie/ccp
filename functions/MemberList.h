#pragma once

#include "../functions/DLL.h"
#include "../functions/RTTIList.h"
#include "../functions/Member.h"

#include "../macros/ClassMacros.h"

#include <QString>

#include <map>
#include <vector>


namespace Common
{
   namespace Functions
   {
      class MemberList final : public RTTIList
      {
         MEMORY(MemberList);

      public:

         virtual bool Contains(const QString& name) const override
         {
            auto it = std::find_if(_Members.begin(), _Members.end(), [&name](auto& kvp) { return kvp.first == name; });
            return it != _Members.end();
         }
         
         virtual std::size_t Size() const override
         {
            return _Members.size();
         }

         MemberList Find(const QString& name)
         {
            MemberList list;
            for (auto& nm : _Members)
               if (nm.first.contains(name))
                  list.Add(nm.second);
            return list;
         }

         void Add(Member::SPtr nm)
         {
            _Members.emplace(nm->Name, nm);
         }

         Member& operator[](const QString& name)
         {
            return *_Members[name];
         }

      private:

         std::map<QString, Member::SPtr> _Members;
      };
   }
}