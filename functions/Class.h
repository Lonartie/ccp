#pragma once

#include "../functions/DLL.h"
#include "../functions/MemberList.h"
#include "../functions/FunctionList.h"

#include "../macros/ClassMacros.h"
#include "../macros/CommonMacros.h"

#include <QString>

namespace Common
{
   namespace Functions
   {
      class Namespace;

      class Class
      {
         MEMORY(Class);

      public:
         QString Name;
         std::shared_ptr<Namespace> Parent;
         MemberList Members;
         FunctionList Functions;
      };
   }
}

#define RTTI_CLASS(x, ...) class x

#define RTTI_CLASS_MAKE_ALIAS(x) \
using __THIS_CLASS__ = x;

#define RTTI_CLASS_MAKE_IDENTIFIER(x) \
static inline const QString __CLASS_ID__ = RTTI_CURRENT_NAMESPACE + QString("::") + QString(STRING(x))

#define RTTI_REGISTER_CLASS(x) \
static inline bool CAT(x, _registered) = (Common::Functions::RTTI::AddClass(__CLASS_ID__, RTTI_CURRENT_NAMESPACE))

#define RTTI_CLASS_GETTER(x) \
static inline std::shared_ptr<Common::Functions::Class> __GET() { return Common::Functions::RTTI::Classes.Get(__CLASS_ID__); }

#define RTTI_SETUP_CLASS(x)                        \
public:                                            \
RTTI_CLASS_MAKE_ALIAS(x);                          \
RTTI_CLASS_MAKE_IDENTIFIER(x);                     \
RTTI_REGISTER_CLASS(x);                            \
RTTI_CLASS_GETTER(x);