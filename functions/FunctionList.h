#pragma once

#include "../functions/DLL.h"
#include "../functions/RTTIList.h"
#include "../functions/Function.h"

#include "../macros/ClassMacros.h"

#include <QString>

#include <map>
#include <vector>


namespace Common
{
   namespace Functions
   {
      class FunctionList final : public RTTIList
      {
         MEMORY(FunctionList);

      public:

         virtual bool Contains(const QString& name) const override
         {
            auto it = std::find_if(_Functions.begin(), _Functions.end(), [&name](auto& kvp) { return kvp.first == name; });
            return it != _Functions.end();
         }

         virtual std::size_t Size() const override
         {
            return _Functions.size();
         }

         FunctionList Find(const QString& name)
         {
            FunctionList list;
            for (auto& nm : _Functions)
               if (nm.first.contains(name))
                  list.Add(nm.second);
            return list;
         }

         void Add(Function::SPtr nm)
         {
            _Functions.emplace(nm->Name, nm);
         }

         Function& operator[](const QString& name)
         {
            return *_Functions[name];
         }

      private:

         std::map<QString, Function::SPtr> _Functions;
      };
   }
}