#pragma once

#include "../functions/DLL.h"
#include "../functions/FunctionObject.h"
#include "../macros/ClassMacros.h"

#include <QString>

#include <cassert>

namespace Common
{
   namespace Functions
   {
      static long __ARGC = 0;

      template<typename Type>
      Type& NextArg(std::vector<FunctionArgument>& args)
      {
         return *static_cast<Type*>(args[__ARGC--].GetValue());
      }

      template<typename RET, typename ...Rest>
      RET CallFunction(RET(*func)(Rest...), std::vector<FunctionArgument>& args)
      {
         return std::invoke(func, NextArg<Rest>(args)...);
      }

      template<typename RET, typename CLS, typename ...Rest>
      RET CallFunction(RET(CLS::* func)(Rest...), std::vector<FunctionArgument>& args)
      {
         CLS* on = static_cast<CLS*>(args[0].GetValue());
         std::vector<FunctionArgument> rest(args.begin() + 1, args.end());
         return std::invoke(func, on, NextArg<Rest>(rest)...);
      }

      template<typename RET, typename CLS>
      RET& CallFunction(RET CLS::* func, std::vector<FunctionArgument>& args)
      {
         CLS* on = static_cast<CLS*>(args[0].GetValue());
         return std::invoke(func, on);
      }

      template<typename RET>
      Common::Functions::FunctionObject::FunctionPointer CreateFunctionPointer(RET * func)
      {
         FunctionObject::FunctionPointer __func = [func](std::vector<FunctionArgument>& args) -> void*
         {
            return func;
         };

         return __func;
      }

      template<typename RET, typename CLS>
      Common::Functions::FunctionObject::FunctionPointer CreateFunctionPointer(RET CLS::* func)
      {
         FunctionObject::FunctionPointer __func = [func](std::vector<FunctionArgument>& args) -> void*
         {
            __ARGC = 0;
            return &(CallFunction(func, args));
         };

         return __func;
      }

      template<typename RET, typename CLS, typename ...ARGS>
      Common::Functions::FunctionObject::FunctionPointer CreateFunctionPointer(RET(CLS::* func)(ARGS...))
      {
         FunctionObject::FunctionPointer __func = [func](std::vector<FunctionArgument>& args) -> void*
         {
            std::size_t ARGS_size = sizeof...(ARGS);
            assert(args.size() == ARGS_size + 1); // +1 because the object we call the function on is first in list!
            __ARGC = sizeof...(ARGS) - 1;

            if constexpr (!std::is_same<RET, void>::value)
            {
               return new RET(CallFunction(func, args));
            } else
            {
               CallFunction(func, args);
               return nullptr;
            }
         };

         return __func;
      }

      template<typename RET, typename ...ARGS>
      Common::Functions::FunctionObject::FunctionPointer CreateFunctionPointer(RET(*func)(ARGS...))
      {
         FunctionObject::FunctionPointer __func = [func](std::vector<FunctionArgument>& args) -> void*
         {
            std::size_t ARGS_size = sizeof...(ARGS);
            assert(args.size() == ARGS_size);
            __ARGC = sizeof...(ARGS) - 1;

            if constexpr (!std::is_same<RET, void>::value)
            {
               return new RET(CallFunction(func, args));
            } else
            {
               CallFunction(func, args);
               return nullptr;
            }
         };

         return __func;
      }
   }
}

#define REGISTER_FUNCTION(x) { Common::Functions::FunctionObject* __obj = new Common::Functions::FunctionObject(#x, Common::Functions::CreateFunctionPointer(&x)); __obj->Register(); }
#define REGISTER_FUNCTION_AS(x, a) { Common::Functions::FunctionObject* __obj = new Common::Functions::FunctionObject(a, Common::Functions::CreateFunctionPointer(&x)); __obj->Register(); }
#define UNREGISTER_ALL_FUNCTIONS() { Common::Functions::FunctionObject::Functions.clear(); }
#define UNREGISTER_FUNCTION(x) { Common::Functions::FunctionObject::Functions[x]->Unregister(); }