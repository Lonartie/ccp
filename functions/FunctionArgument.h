#pragma once

#include "../functions/DLL.h"
#include "../macros/ClassMacros.h"

#include <QString>

namespace Common
{
   namespace Functions
   {
      class FUNCTIONS_EXPORT FunctionArgument
      {
         MEMORY(FunctionArgument);

      public /*OBJECT*/:

         FunctionArgument();
         FunctionArgument(const QString& name);
         FunctionArgument(void* value);
         FunctionArgument(const QString& name, void* value);

      public /*FUNCTIONS*/:

         const QString& GetName() const;
         void* GetValue();
         void SetValue(void* value);

      public /*OPERATORS*/:

         void* operator*();

      private /*FUNCTIONS*/:

      private /*MEMBERS*/:

         QString m_name = "";
         void* m_value = nullptr;
      };
   }
}

