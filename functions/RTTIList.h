#pragma once
#include "../functions/DLL.h"
#include "../macros/ClassMacros.h"
#include "QString"

namespace Common
{
   namespace Functions
   {
      class RTTIList
      {
         MEMORY(RTTIList);

         virtual bool Contains(const QString& name) const = 0;
         virtual std::size_t Size() const = 0;
      };
   }
}