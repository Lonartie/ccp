#pragma once

#include "../functions/DLL.h"
#include "../functions/RTTIList.h"
#include "../functions/Class.h"

#include "../macros/ClassMacros.h"

#include <QString>

#include <map>
#include <vector>


namespace Common
{
   namespace Functions
   {
      class ClassesList final : public RTTIList
      {
         MEMORY(ClassesList);

      public:

         virtual bool Contains(const QString& name) const override
         {
            auto it = std::find_if(_Classes.begin(), _Classes.end(), [&name](auto& kvp) { return kvp.first == name; });
            return it != _Classes.end();
         }
         
         virtual std::size_t Size() const override
         {
            return _Classes.size();
         }

         ClassesList Find(const QString& name)
         {
            ClassesList list;
            for (auto& nm : _Classes)
               if (nm.first.contains(name))
                  list.Add(nm.second);
            return list;
         }

         void Add(Class::SPtr nm)
         {
            _Classes.emplace(nm->Name, nm);
         }

         void Insert(const QString& name, Class::SPtr nm)
         {
            _Classes.emplace(name, nm);
         }

         Class::SPtr Get(const QString& name)
         {
            return _Classes[name];
         }

         Class& operator[](const QString& name)
         {
            return *_Classes[name];
         }

      private:

         std::map<QString, Class::SPtr> _Classes;
      };
   }
}