#pragma once

#include "../functions/DLL.h"

#include "../macros/ClassMacros.h"

#include <QString>
#include "FunctionObject.h"

namespace Common
{
   namespace Functions
   {
      class Class;

      class Function
      {
         MEMORY(Function);

      public:
         QString Name;
         std::shared_ptr<Class> Parent;
         FunctionObject CallingFunction;

         template<typename T = void, typename ...Args>
         T* Call(Args...args)
         {
            return CallingFunction.Call<T>(args...);
         }

         template<typename T = void, typename U, typename ...Args>
         T* CallOn(U& on, Args...args)
         {
            return CallingFunction.CallOn<T>(&on, args...);
         }
      };
   }
}

#define RTTI_REGISTER_FUNCTION(x) ([]()                              \
{                                                                    \
   auto shared = Common::Functions::Function::CreateShared();        \
   shared->Name = STRING(x);                                         \
   shared->Parent = __GET();                                         \
   shared->CallingFunction = FunctionObject(STRING(x),               \
   Common::Functions::CreateFunctionPointer(&__THIS_CLASS__::x));    \
   __GET()->Functions.Add(shared);                                   \
   return true;                                                      \
}())

#define RTTI_FUNCTION_BEGIN(RET, NAME, ARGS, ACC) RET NAME ARGS ACC

#define RTTI_FUNCTION_END(NAME) static inline bool CAT(NAME, _registered) = RTTI_REGISTER_FUNCTION(NAME); 