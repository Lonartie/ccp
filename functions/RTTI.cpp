#include "RTTI.h"
#include "Namespace.h"

Common::Functions::NamespacesList Common::Functions::RTTI::Namespaces;
Common::Functions::ClassesList Common::Functions::RTTI::Classes;

/************************************************************************/
/*                         RTTI INTERNAL STUFF                          */
/************************************************************************/

QString Common::Functions::RTTI::__Current_Namespace__;

bool Common::Functions::RTTI::AddNamespace(const QString& nms)
{
   if (!Namespaces.Contains(nms))
   {
      auto shared = Common::Functions::Namespace::CreateShared();
      shared->Name = nms;
      Namespaces.Add(shared);
   }
   return true;
}

bool Common::Functions::RTTI::AddClass(const QString& cls, const QString& nms)
{
   if (!Classes.Contains(cls))
   {
      auto shared = Common::Functions::Class::CreateShared();
      shared->Name = cls;
      shared->Parent = Namespaces.Get(nms);
      shared->Parent->Classes.Insert(QString(cls).remove(nms).remove("::"), shared);
      Classes.Add(shared);
   }
   return true;
}
