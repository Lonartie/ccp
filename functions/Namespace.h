#pragma once
#include "../functions/DLL.h"
#include "../functions/ClassesList.h"

#include "../macros/ClassMacros.h"
#include "../macros/CommonMacros.h"

#include <QString>
#include <QStringList>

namespace Common
{
   namespace Functions
   {
      class Namespace
      {
         MEMORY(Namespace);

      public:
         QString Name;
         ClassesList Classes;
      };
   }
}

#define RTTI_NAMESPACE_BEGIN(x) namespace x                                                  \
{                                                                                            \
   auto CAT(x, _entered) = RTTI_ENTER_NAMESPACE(STRING(x));                                  \
   auto CAT(x, _registered) = RTTI_REGISTER_NAMESPACE(RTTI_CURRENT_NAMESPACE); 

#define RTTI_NAMESPACE_END() auto CAT(x, _leaved) = RTTI_LEAVE_NAMESPACE(); }

#define RTTI_CURRENT_NAMESPACE Common::Functions::RTTI::__Current_Namespace__

#define RTTI_REGISTER_NAMESPACE(x) (Common::Functions::RTTI::AddNamespace(x))

#define RTTI_ENTER_NAMESPACE(__namespace) ([]()                                              \
{                                                                                            \
   RTTI_CURRENT_NAMESPACE += QString("::") + QString(__namespace);                           \
   return true;                                                                              \
}())

#define RTTI_LEAVE_NAMESPACE() ([]()                                                         \
{                                                                                            \
   QStringList list = RTTI_CURRENT_NAMESPACE.split("::");                                    \
   RTTI_CURRENT_NAMESPACE.clear();                                                           \
   for (int i = 0; i < list.size() - 1; i++)                                                 \
      RTTI_CURRENT_NAMESPACE += list[i] + (i == list.size() - 2 ? "" : "::");                \
   return true;                                                                              \
}())