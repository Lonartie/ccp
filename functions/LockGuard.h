#pragma once
#include <mutex>

namespace Common
{
   namespace Functions
   {
      // HEADER ONLY

      /*
      	#################### DECLARATION ####################
      */

      template<typename T>
      class LockGuard
      {
      public:
         LockGuard() = delete;
         LockGuard(const LockGuard&) = delete;
         LockGuard(LockGuard&&) = delete;

         LockGuard(const T& val) noexcept;
         LockGuard(T&& val) noexcept;
         ~LockGuard();

         LockGuard& operator=(const T& val);
         LockGuard& operator=(T&& val);
         operator const T& ();

      private:
         std::mutex val_mutex;
         T val;
      };

      /*
      	#################### IMPLEMENTATION ####################
      */

      template <typename T>
      LockGuard<T>::LockGuard(const T& val) noexcept: val(val)
      {}

      template <typename T>
      LockGuard<T>::LockGuard(T&& val) noexcept: val(val)
      {}

      template <typename T>
      LockGuard<T>::~LockGuard() = default;

      template <typename T>
      LockGuard<T>& LockGuard<T>::operator=(const T& val)
      {
         val_mutex.lock();
         this->val = val;
         val_mutex.unlock();
         return *this;
      }

      template <typename T>
      LockGuard<T>& LockGuard<T>::operator=(T&& val)
      {
         val_mutex.lock();
         this->val = std::move(val);
         val_mutex.unlock();
         return *this;
      }

      template <typename T>
      LockGuard<T>::operator const T& ()
      {
         return val;
      }
   }
}