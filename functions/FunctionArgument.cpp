#include "FunctionArgument.h"

Common::Functions::FunctionArgument::FunctionArgument()
   : m_name("") { }

Common::Functions::FunctionArgument::FunctionArgument(const QString& name, void* value)
   : m_name(name)
   , m_value(value) {}

Common::Functions::FunctionArgument::FunctionArgument(void* value)
   : m_value(value) {}

Common::Functions::FunctionArgument::FunctionArgument(const QString& name)
   : m_name(name) {}


const QString& Common::Functions::FunctionArgument::GetName() const
{
   return m_name;
}

void* Common::Functions::FunctionArgument::GetValue()
{
   return m_value;
}

void Common::Functions::FunctionArgument::SetValue(void* value)
{
   m_value = value;
}

void* Common::Functions::FunctionArgument::operator*()
{
   return m_value;
}
