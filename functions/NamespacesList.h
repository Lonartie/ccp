#pragma once

#include "../functions/DLL.h"
#include "../functions/RTTIList.h"
#include "../functions/Namespace.h"

#include "../macros/ClassMacros.h"

#include <QString>

#include <map>
#include <vector>


namespace Common
{
   namespace Functions
   {
      class NamespacesList final : public RTTIList
      {
         MEMORY(NamespacesList);

      public:

         virtual bool Contains(const QString& name) const override
         {
            auto it = std::find_if(_Namespaces.begin(), _Namespaces.end(), [&name](auto& kvp) { return kvp.first == name; });
            return it != _Namespaces.end();
         }

         virtual std::size_t Size() const override
         {
            return _Namespaces.size();
         }

         NamespacesList Find(const QString& name)
         {
            NamespacesList list;
            for (auto& nm : _Namespaces)
               if (nm.first.contains(name))
                  list.Add(nm.second);
            return list;
         }

         void Add(Namespace::SPtr nm)
         {
            _Namespaces.emplace(nm->Name, nm);
         }

         Namespace::SPtr Get(const QString& name)
         {
            return _Namespaces[name];
         }

         Namespace& operator[](const QString& name)
         {
            return *_Namespaces[name];
         }

      private:

         std::map<QString, Namespace::SPtr> _Namespaces;
      };
   }
}