#pragma once

#include "../functions/DLL.h"
#include "../functions/NamespacesList.h"
#include "../functions/ClassesList.h"

#include "../macros/ClassMacros.h"


namespace Common
{
   namespace Functions
   {
      class FUNCTIONS_EXPORT RTTI
      {
         MEMORY(RTTI);

      public:
         static NamespacesList Namespaces;
         static ClassesList Classes;
         static QString __Current_Namespace__;

         static bool AddNamespace(const QString& nms);
         static bool AddClass(const QString& cls, const QString& nms);
      };
   }
}