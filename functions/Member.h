#pragma once

#include "../functions/DLL.h"
#include "../functions/FunctionObject.h"

#include "../macros/ClassMacros.h"
#include "../macros/CommonMacros.h"

#include <QString>

namespace Common
{
   namespace Functions
   {
      class Class;

      class Member
      {
         MEMORY(Member);

      public:
         QString Name;
         std::shared_ptr<Class> Parent;
         FunctionObject CallingFunction;

         template<typename T, typename U>
         T& GetValue(U& obj)
         {
            return *CallingFunction.CallOn<T>(&obj);
         }

         template<typename T>
         T& GetValue()
         {
            return *CallingFunction.Call<T>();
         }
      };
   }
}

#define RTTI_REGISTER_MEMBER(x) ([]()                                \
{                                                                    \
   auto shared = Common::Functions::Member::CreateShared();          \
   shared->Name = STRING(x);                                         \
   shared->Parent = __GET();                                         \
   shared->CallingFunction = FunctionObject(STRING(x),                      \
   Common::Functions::CreateFunctionPointer(&__THIS_CLASS__::x));    \
   __GET()->Members.Add(shared);                                     \
   return true;                                                      \
}())

#define RTTI_MEMBER(TYPE, NAME) TYPE NAME; static inline bool CAT(NAME, _registered) = RTTI_REGISTER_MEMBER(NAME);