#include "FunctionObject.h"

std::map<QString, Common::Functions::FunctionObject*> Common::Functions::FunctionObject::Functions;

bool Common::Functions::FunctionObject::IsRegistered(const QString& name)
{
   auto it = std::find_if(Functions.begin(), Functions.end(), [&name](auto& kvp) { return kvp.first == name; });
   return it != Functions.end();
}

Common::Functions::FunctionObject::FunctionObject()
   : m_name("")
   , m_function(nullptr)
{}

Common::Functions::FunctionObject::FunctionObject(const QString& name, FunctionPointer function)
   : m_name(name)
   , m_function(function) 
{
}

const QString& Common::Functions::FunctionObject::GetName() const
{
   return m_name;
}

void Common::Functions::FunctionObject::SetName(const QString& name)
{
   m_name = name;
}

const Common::Functions::FunctionObject::FunctionPointer Common::Functions::FunctionObject::GetFunction() const
{
   return m_function;
}

void Common::Functions::FunctionObject::SetFunction(FunctionPointer function)
{
   m_function = function;
}

void Common::Functions::FunctionObject::Register()
{
   Functions.emplace(m_name, this);
}

void Common::Functions::FunctionObject::Unregister()
{
   auto it = std::find_if(Functions.begin(), Functions.end(), [&](auto& kvp) { return kvp.first == m_name; });
   if (it != Functions.end()) 
      Functions.erase(it);
}
