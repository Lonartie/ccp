#pragma once

#include "../functions/DLL.h"
#include "../functions/FunctionArgument.h"
#include "../macros/ClassMacros.h"

#include <QString>

#include <type_traits>
#include <vector>
#include <functional>
#include <map>

namespace Common
{
   namespace Functions
   {
      class FUNCTIONS_EXPORT FunctionObject
      {
         MEMORY(FunctionObject);

      public /*STATIC*/:

         static std::map<QString, FunctionObject*> Functions;

         static bool IsRegistered(const QString& name);

      public /*OBJECT*/:

         using FunctionPointer = std::function<void*(std::vector<FunctionArgument>&)>;

         FunctionObject();

         FunctionObject(const QString& name, FunctionPointer function);

         ~FunctionObject() = default;

      public /*FUNCTIONS*/:

         template<typename OUT, typename ...Args>
         OUT* Call(Args...args);

         template<typename OUT, typename ...Args>
         OUT* CallOn(void* on, Args...args);

         const QString& GetName() const;
         void SetName(const QString& name);

         const FunctionPointer GetFunction() const;
         void SetFunction(FunctionPointer function);
         
         void Register();
         void Unregister();

      private /*MEMBERS*/:

         QString m_name;
         FunctionPointer m_function;

      };

      /************************************************************************/
      /*                              DEFINITIONS                             */
      /************************************************************************/

      template<typename OUT, typename ...Args>
      OUT* Common::Functions::FunctionObject::Call(Args...args)
      {
         if (!m_function) return nullptr;
         return static_cast<OUT*>(std::invoke(m_function, std::vector<FunctionArgument> { (&args)... }));
      }

      template<typename OUT, typename ...Args>
      OUT* Common::Functions::FunctionObject::CallOn(void* on, Args...args)
      {
         if (!m_function) return nullptr;
         return static_cast<OUT*>(std::invoke(m_function, std::vector<FunctionArgument> { on, (&args)... }));
      }
   }
}

namespace Common
{
   namespace Functions
   {
      template<typename OUT=void, typename ...ARGS>
      OUT* Call(const QString& name, ARGS...args)
      {
         auto& funcs = Common::Functions::FunctionObject::Functions;
         auto it = std::find_if(funcs.begin(), funcs.end(), [&name](auto& kvp) { return kvp.first == name; });
         if (it == funcs.end()) return nullptr;
         return funcs[name]->Call<OUT>(args...);
      }

      template<typename OUT = void, typename ...ARGS>
      OUT* CallOn(void* on, const QString& name, ARGS...args)
      {
         auto& funcs = Common::Functions::FunctionObject::Functions;
         auto it = std::find_if(funcs.begin(), funcs.end(), [&name](auto& kvp) { return kvp.first == name; });
         if (it == funcs.end()) return nullptr;
         return funcs[name]->CallOn<OUT>(on, args...);
      }
   }
}