#pragma once

#include <QByteArray>

namespace Common
{
   namespace Functions
   {
      template<typename T>
      char* ToRaw(T* data)
      {
         return static_cast<char*>(static_cast<void*>(data));
      }
      template<typename T>
      const char* ToRaw(const T* data)
      {
         return static_cast<const char*>(static_cast<const void*>(data));
      }

      template<typename T>
      T* FromRaw(char* data)
      {
         return static_cast<T*>(static_cast<void*>(data));
      }
      template<typename T>
      const T* FromRaw(const char* data)
      {
         return static_cast<const T*>(static_cast<const void*>(data));
      }

      template<typename T>
      QByteArray ToQByteArray(const T* data, const std::size_t size)
      {
         return QByteArray(ToRaw(data), size * sizeof(T));
      }

      template<typename T>
      const T* FromQByteArray(const QByteArray& data)
      {
         return Functions::FromRaw<T>(data.data());
      }
      template<typename T>
      T* FromQByteArray(QByteArray& data)
      {
         return Functions::FromRaw<T>(data.data());
      }
   }
}