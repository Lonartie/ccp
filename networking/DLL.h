#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(NETWORKING_LIB)
#  define NETWORKING_EXPORT Q_DECL_EXPORT
# else
#  define NETWORKING_EXPORT Q_DECL_IMPORT
# endif
#else
# define NETWORKING_EXPORT
#endif
