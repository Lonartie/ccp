#pragma once
#include <string>

#define THROW(str) (throw std::exception(std::string(str).c_str()))
#define THROW_IF(fnc, str) { if((fnc)) {THROW(str); } }