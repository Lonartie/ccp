#pragma once

// std
#include <memory>

#define MEMORY(class_name) \
public: \
using SPtr = std::shared_ptr<class_name>; \
using UPtr = std::unique_ptr<class_name>; \
template<typename ...Args> static SPtr CreateShared(Args...args) { return std::make_shared<class_name>(args...); } \
template<typename ...Args> static UPtr CreateUnique(Args...args) { return std::make_unique<class_name>(args...); } \
private:

#define UNIQUE(type, ...) std::make_unique<type>((__VA_ARGS__))
#define SHARED(type, ...) std::make_shared<type>((__VA_ARGS__))

#define TYPEOF(name) std::remove_volatile<std::remove_const<std::remove_reference<std::remove_pointer<decltype(name)>::type>::type>::type>::type
#define BASE TYPEOF(this)

#define _connect(a, func_a, b, func_b) connect(a, &TYPEOF(a)::func_a, b, &TYPEOF(b)::func_b)
#define NEW_PTR(x) new TYPEOF(x) (x)