#pragma once

#define _STRING(x) #x
#define STRING(x) _STRING(x)

#define _CAT(a, b) a##b
#define CAT(a, b) _CAT(a, b)