#pragma once
#include <functional>

#define TO_GLOBAL(fnc, arg_count) (std::bind(fnc, this, std::placeholders::_##arg_count))