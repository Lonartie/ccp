#include "ClProgram.h"

#include "macros/Exceptions.h"

#include <CL/cl.hpp>

#include <QtCore/QFile>

#include <functional>
#include "macros/Functions.h"

using namespace cl;

namespace
{
   template<typename T>
   T& To(std::shared_ptr<void> ptr)
   {
      return *static_cast<T*>(ptr.get());
   }
}

Common::Calculations::ClProgram::ClProgram(QObject* parent)
   : QObject(parent)
   , lastClError(0)
   , processed_includes(0)
{}

void Common::Calculations::ClProgram::LoadDataFromContent(const QString& content)
{
   program_source = content;
}

void Common::Calculations::ClProgram::LoadDataFromFile(const QString& file)
{
   QFile _file(file);
   _file.open(QIODevice::ReadOnly);
   program_source = _file.readAll();
   _file.close();
}

void Common::Calculations::ClProgram::AddIncludesFromContent(const QString& content)
{
   include_sources.Add(content);
}

void Common::Calculations::ClProgram::AddIncludesFromFile(const QString& file)
{
   QFile _file(file);
   _file.open(QIODevice::ReadOnly);
   include_sources.Add(_file.readAll());
   _file.close();
}

void Common::Calculations::ClProgram::SetDeviceType(DeviceType deviceType /*= CPU*/)
{
   Linq<Platform> platforms;
   lastClError = Platform::get(&platforms.Original());
   ReportError();

   Linq<Device> devices;

   lastClError = platforms.First().getDevices(deviceType == CPU ? CL_DEVICE_TYPE_CPU :
                                              deviceType == GPU ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_ALL,
                                              &devices.Original());
   ReportError();

   cl_device = SHARED(Device, devices.First());
}

bool Common::Calculations::ClProgram::Compile(Linq<QString> macros)
{
   include_sources.Foreach(TO_GLOBAL(&ClProgram::SaveInclude, 1));
   processed_includes = 0;
   const QString include_dir(temp_dir.path());
   const QString include_command(QString("-I %1").arg(include_dir));
   const Linq<QString> macro_commands = macros.SelectInPlace(&ClProgram::MacroToCommand);
   const QString command_list = include_command + macro_commands.Accumulate(QString(), &ClProgram::MacrosToSingleCommand);

   cl_context = SHARED(Context, 
                       To<Device>(cl_device));

   cl_program = SHARED(Program, 
                       To<Context>(cl_context), 
                       program_source.toStdString());

   if(To<Program>(cl_program).build(command_list.toStdString().c_str()))
   {
      buildLog = QString::fromStdString(To<Program>(cl_program).getBuildInfo<CL_PROGRAM_BUILD_LOG>(To<Device>(cl_device)));
      return false;
   }

   return true;
}

const QString& Common::Calculations::ClProgram::GetLastCompilerError() const
{
   return lastCompileError;
}

const QString& Common::Calculations::ClProgram::GetBuildLog() const
{
   return buildLog;
}

void Common::Calculations::ClProgram::ReportError() const
{
   THROW_IF(lastClError != 0, "ClError: " + std::to_string(lastClError));
}

void Common::Calculations::ClProgram::SaveInclude(const QString& include)
{
   const QString name = QString("include_files_%1").arg(processed_includes++);
   const QFileInfo fileInfo(temp_dir.filePath(name));
   QFile file(fileInfo.absoluteFilePath());
   file.open(QIODevice::WriteOnly);
   file.write(include.toLatin1());
   file.flush();
   file.close();
}

QString Common::Calculations::ClProgram::MacroToCommand(const QString& macro)
{
   return QString("-D %1").arg(macro);
}

void Common::Calculations::ClProgram::MacrosToSingleCommand(QString& list, const QString& macro)
{
   list += QString(" %1").arg(macro);
}
