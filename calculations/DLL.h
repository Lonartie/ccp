#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(CALCULATIONS_LIB)
#  define CALCULATIONS_EXPORT Q_DECL_EXPORT
# else
#  define CALCULATIONS_EXPORT Q_DECL_IMPORT
# endif
#else
# define CALCULATIONS_EXPORT
#endif
