#pragma once

#include "DLL.h"

#include "macros/ClassMacros.h"
#include "modules/linq/LinqSln/Linq/Linq.h"

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QTemporaryDir>

namespace Common
{
   namespace Calculations
   {
      class CALCULATIONS_EXPORT ClProgram: public QObject
      {
         Q_OBJECT;
         MEMORY(ClProgram);

      public:

         enum DeviceType
         {
            CPU,
            GPU
         };

         ClProgram(QObject* parent = nullptr);
         ~ClProgram() = default;

         void LoadDataFromContent(const QString& content);
         void LoadDataFromFile(const QString& file);

         void AddIncludesFromContent(const QString& content);
         void AddIncludesFromFile(const QString& file);

         void SetDeviceType(DeviceType deviceType = CPU);

         bool Compile(Common::Linq<QString> macros = {});
         const QString& GetLastCompilerError() const;

         const QString& GetBuildLog() const;

      private:

         void ReportError() const;

         void SaveInclude(const QString& include);

         static QString MacroToCommand(const QString& macro);
         static void MacrosToSingleCommand(QString& list, const QString& macro);

         QTemporaryDir
            temp_dir;

         QString
            program_source,
            lastCompileError,
            buildLog;

         Common::Linq<QString>
            include_sources;

         int
            lastClError,
            processed_includes;

         std::shared_ptr<void>
            cl_device,
            cl_program,
            cl_context;
      };
   }
}
