#include <boost/test/unit_test.hpp>
#include <QString>
#include "calculations/ClProgram.h"
using namespace Common;
using namespace Common::Calculations;

BOOST_AUTO_TEST_SUITE(TS_Calculations_ClProgram)

BOOST_AUTO_TEST_CASE(TestProgramBuilds)
{
   BOOST_CHECK_NO_THROW([]() 
   {
      QString program = "kernel void main(;) {}";
      ClProgram clProgram;
      clProgram.SetDeviceType(ClProgram::DeviceType::GPU);
      clProgram.LoadDataFromContent(program);
      BOOST_CHECK_EQUAL(true, clProgram.Compile());
      BOOST_TEST_MESSAGE(clProgram.GetBuildLog().toStdString());
   }());
}

BOOST_AUTO_TEST_SUITE_END()