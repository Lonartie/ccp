#include <boost/test/unit_test.hpp>
#include <QString>
#include <windows.h>
#include "calculations/ClProgram.h"
#include <QFileInfo>
#include <QDirIterator>
using namespace Common;
using namespace Common::Calculations;

BOOST_AUTO_TEST_SUITE(TS_Calculations_ClProgram)

BOOST_AUTO_TEST_CASE(TestProgramBuilds)
{
   BOOST_CHECK_NO_THROW([]()
   {
      QString program = "kernel void main() {int z = 0;}";
      ClProgram clProgram;
      clProgram.SetDeviceType(ClProgram::DeviceType::CPU);
      clProgram.LoadDataFromContent(program);
      BOOST_CHECK_EQUAL(true, clProgram.Compile());
      BOOST_TEST_MESSAGE(clProgram.GetBuildLog().toStdString());
   }());
}

BOOST_AUTO_TEST_CASE(AllClFilesCompile)
{
   WCHAR buffer[MAX_PATH];
   unsigned long size = 0;
   auto acsize = GetModuleFileName(nullptr, buffer, MAX_PATH);
   QString file = QString::fromWCharArray(buffer, acsize);
   QFileInfo info(file);
   QString path(info.absolutePath());
   QDir dir(path);

   QDirIterator it(dir.absolutePath(), QStringList() << "*.cl", QDir::Files | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);
   ClProgram prgm;
   prgm.SetDeviceType(ClProgram::DeviceType::CPU);

   while(it.hasNext())
   {
      QString filePath = it.next();
      QFile file(filePath);
      file.open(QIODevice::ReadOnly);
      prgm.LoadDataFromContent(file.readAll());
      file.close();

      BOOST_CHECK_EQUAL(true, prgm.Compile());
      BOOST_TEST_MESSAGE(filePath.toStdString() << ":\n" << prgm.GetBuildLog().toStdString());
   }
}

BOOST_AUTO_TEST_SUITE_END()