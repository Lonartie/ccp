#include <boost/test/unit_test.hpp>
#include <functional>
#include "../functions/LockGuard.h"
using namespace Common::Functions;


BOOST_AUTO_TEST_SUITE(TS_Functions_LockGuard)

BOOST_AUTO_TEST_CASE(LockGuardIsThreadSafe)
{
   LockGuard<unsigned long long> _test = 2;

   std::function<void()> fc = [&]() {

      std::function<void()> calc = [&]()
      {
         for (unsigned long long i = 0; i < 1e5; i++)
         {
            _test = i;
         }
      };

      std::thread a(calc);
      std::thread b(calc);

      a.join();
      b.join();
   };

   BOOST_CHECK_NO_THROW(fc());

}

BOOST_AUTO_TEST_SUITE_END();
