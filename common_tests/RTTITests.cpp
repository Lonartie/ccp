#include <boost/test/unit_test.hpp>
#include "../functions/FunctionObject.h"
#include "../functions/FunctionLinker.h"
#include "../functions/RTTI.h"

#include <string>
#include "QStringList"

using namespace Common::Functions;

auto print = [](const QString& msg) { std::cout << msg.toStdString() << std::endl; return true; };
auto pause = []() { std::cin.get(); return true; };

BOOST_AUTO_TEST_SUITE(TS_RTTI)

template<typename T> T add(T a, T b) { return a + b; }
template<typename T> T sub(T a, T b) { return a - b; }

BOOST_AUTO_TEST_CASE(RTTI_FunctionObjectCalling)
{
   REGISTER_FUNCTION(add<int>);
   REGISTER_FUNCTION(sub<int>);

   BOOST_CHECK_EQUAL(2, *Call<int>("add<int>", 1, 1));
   BOOST_CHECK_EQUAL(0, *Call<int>("sub<int>", 1, 1));

   REGISTER_FUNCTION_AS(add<double>, "add_double");
   REGISTER_FUNCTION_AS(sub<double>, "sub_double");

   BOOST_CHECK_EQUAL(2.0, *Call<double>("add_double", 1.0, 1.0));
   BOOST_CHECK_EQUAL(0.0, *Call<double>("sub_double", 1.0, 1.0));
   UNREGISTER_ALL_FUNCTIONS();
}

BOOST_AUTO_TEST_CASE(RTTI_StaticClassFunctionCalling)
{
   struct _test
   {
      static int add(int a, int b) { return a + b; }
      static int sub(int a, int b) { return a - b; }
   };

   REGISTER_FUNCTION_AS(_test::add, "add");
   REGISTER_FUNCTION_AS(_test::sub, "sub");

   BOOST_CHECK_EQUAL(10, *Call<int>("add", 9, 1));
   BOOST_CHECK_EQUAL(8, *Call<int>("sub", 9, 1));
   UNREGISTER_ALL_FUNCTIONS();
}

BOOST_AUTO_TEST_CASE(RTTI_ClassMemberFunctionCalling)
{
   struct _test
   {
      int add(int a, int b) { return a + b; }
      int sub(int a, int b) { return a - b; }
      QString name;
      QString GetName() { return name; }
   };

   REGISTER_FUNCTION_AS(_test::add, "add");
   REGISTER_FUNCTION_AS(_test::sub, "sub");
   REGISTER_FUNCTION_AS(_test::name, "Name"); // also directly members

   BOOST_CHECK_EQUAL(10, *CallOn<int>(&_test(), "add", 9, 1));
   BOOST_CHECK_EQUAL(8, *CallOn<int>(&_test(), "sub", 9, 1));

   _test test;
   test.name = "Hello World!";
   BOOST_CHECK_EQUAL("Hello World!", CallOn<QString>(&test, "Name")->toStdString());
   UNREGISTER_ALL_FUNCTIONS();
}

BOOST_AUTO_TEST_CASE(RTTI_UnregisterWorks)
{
   auto& funcs = FunctionObject::Functions;
   REGISTER_FUNCTION(add<int>);

   BOOST_CHECK_EQUAL(true, FunctionObject::IsRegistered("add<int>"));
   BOOST_CHECK_EQUAL(1, funcs.size());

   UNREGISTER_ALL_FUNCTIONS();

   BOOST_CHECK_EQUAL(false, FunctionObject::IsRegistered("add<int>"));
   BOOST_CHECK_EQUAL(0, funcs.size());

   REGISTER_FUNCTION(add<int>);

   BOOST_CHECK_EQUAL(true, FunctionObject::IsRegistered("add<int>"));
   BOOST_CHECK_EQUAL(1, funcs.size());

   UNREGISTER_FUNCTION("add<int>");

   BOOST_CHECK_EQUAL(false, FunctionObject::IsRegistered("add<int>"));
   BOOST_CHECK_EQUAL(0, funcs.size());
   UNREGISTER_ALL_FUNCTIONS();
}

BOOST_AUTO_TEST_CASE(RTTI_ChainCalling)
{
   static bool called = false;
   struct _t { void a() { CallOn(this, "_t::b"); } void b() { called = true; } };

   REGISTER_FUNCTION(_t::a);
   REGISTER_FUNCTION(_t::b);
   auto i = Common::Functions::Function::CreateShared();
   CallOn(&_t(), "_t::a");
   BOOST_CHECK_EQUAL(true, called);
   UNREGISTER_ALL_FUNCTIONS();
}

struct _base {};

RTTI_NAMESPACE_BEGIN(_namespace)

RTTI_CLASS(_class): public _base
{
   RTTI_SETUP_CLASS(_class);

   RTTI_MEMBER(int, a);
   RTTI_MEMBER(int*, b);

   RTTI_FUNCTION_BEGIN(int, GetA, ())
   {
      return a;
   } RTTI_FUNCTION_END(GetA)
};

RTTI_NAMESPACE_END()

BOOST_AUTO_TEST_CASE(RTTI_TestClassInfo)
{
   auto& nms = RTTI::Namespaces;
   auto& cls = RTTI::Classes;

   BOOST_CHECK_EQUAL(true, RTTI::Namespaces.Contains("::_namespace"));
   BOOST_CHECK_EQUAL(1, RTTI::Classes.Find("_class").Size());
   BOOST_CHECK_EQUAL(true, RTTI::Classes.Contains("::_namespace::_class"));
   BOOST_CHECK_EQUAL(2, RTTI::Classes["::_namespace::_class"].Members.Size());
   BOOST_CHECK_EQUAL(true, RTTI::Classes["::_namespace::_class"].Members.Contains("a"));
   BOOST_CHECK_EQUAL(0, RTTI::Classes["::_namespace::_class"].Members["a"].GetValue<int>(_namespace::_class()));
   BOOST_CHECK_EQUAL(0, *RTTI::Classes["::_namespace::_class"].Functions["GetA"].CallOn<int>(_namespace::_class()));

   _namespace::_class obj;
   obj.a = 9;
   obj.b = &obj.a;

   BOOST_CHECK_EQUAL(9, RTTI::Classes["::_namespace::_class"].Members["a"].GetValue<int>(obj));
   RTTI::Namespaces["::_namespace"].Classes["_class"].Members["a"].GetValue<int>(obj) = 10;
   BOOST_CHECK_EQUAL(10, RTTI::Classes["::_namespace::_class"].Members["a"].GetValue<int>(obj));
   BOOST_CHECK_EQUAL(10, obj.a);
   BOOST_CHECK_EQUAL(10, *obj.b);
}

RTTI_NAMESPACE_BEGIN(MATH);

RTTI_CLASS(CALC)
{
   RTTI_SETUP_CLASS(CALC);

public:

   CALC() { instances++; }
   ~CALC() { instances--; }

   static RTTI_MEMBER(std::size_t, instances);
   static RTTI_FUNCTION_BEGIN(int, add, (int a, int b))
   {
      return a + b;
   } RTTI_FUNCTION_END(add);
};

std::size_t CALC::instances;

RTTI_NAMESPACE_END();

BOOST_AUTO_TEST_CASE(RTTI_SimpleClass)
{
   BOOST_CHECK_EQUAL(15, *RTTI::Classes["::MATH::CALC"].Functions["add"].Call<int>(5, 10));
   MATH::CALC _1;
   MATH::CALC _2;
   MATH::CALC _3;
   BOOST_CHECK_EQUAL(3, RTTI::Classes["::MATH::CALC"].Members["instances"].GetValue<std::size_t>());
}

BOOST_AUTO_TEST_SUITE_END()