#include <boost/test/unit_test.hpp>
#include "../modules/Linq/LinqSln/Linq/Linq.h"

using namespace Common;

BOOST_AUTO_TEST_SUITE(TS_QStringLinq)

BOOST_AUTO_TEST_CASE(QStringLinq_ConcatenateWithSeparator)
{
   auto list = CreateLinq<QString>().Add("1").Add("2").Add("3");
   BOOST_CHECK_EQUAL("1,2,3", list.ConcatenateWithSeparator(",").toStdString());
   BOOST_CHECK_EQUAL("1-2-3", list.ConcatenateWithSeparator("-").toStdString());
   BOOST_CHECK_EQUAL("2-3", list.Remove("1").ConcatenateWithSeparator("-").toStdString());
   BOOST_CHECK_EQUAL("2", list.Remove("3").ConcatenateWithSeparator("-").toStdString());
   BOOST_CHECK_EQUAL("2o1o3", list.Add("1").Add("3").ConcatenateWithSeparator("o").toStdString());
   BOOST_CHECK_EQUAL("2_._1_._3", list.ConcatenateWithSeparator("_._").toStdString());
}

BOOST_AUTO_TEST_SUITE_END()