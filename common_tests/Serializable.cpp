#include <boost/test/unit_test.hpp>
#include "storage/Serializable.h"
#include "storage/StorageBackend.h"
#include "storage/MemoryStorageBackend.h"
#include "storage/HardDriveStorageBackend.h"

using namespace Common::Storage;

namespace
{
   struct simple: public Serializable<simple>
   {
      simple(): Serializable<simple>(this) {};
      int a, b, c;
      long d, e, f;
   };
}

BOOST_AUTO_TEST_SUITE(TS_Storage_Serializable)

BOOST_AUTO_TEST_CASE(SimpleStruct_MemorySerialization)
{
   Storage<MemoryStorageBackend> memory;

   simple in;
   in.a = 1;
   in.b = 2;
   in.c = 3;
   in.d = 4;
   in.e = 5;
   in.f = 6;

   in.Serialize(memory);
   auto out = simple::Deserialize(memory);

   BOOST_CHECK_EQUAL(in.a, out.a);
   BOOST_CHECK_EQUAL(in.b, out.b);
   BOOST_CHECK_EQUAL(in.c, out.c);
   BOOST_CHECK_EQUAL(in.d, out.d);
   BOOST_CHECK_EQUAL(in.e, out.e);
   BOOST_CHECK_EQUAL(in.f, out.f);
}

BOOST_AUTO_TEST_CASE(SimpleStruct_HardDriveSerialization)
{
   Storage<HardDriveStorageBackend> storage("C:/Users/Lonar/Desktop/test.bin");

   simple in;
   in.a = 1;
   in.b = 2;
   in.c = 3;
   in.d = 4;
   in.e = 5;
   in.f = 6;

   in.Serialize(storage);
   auto out = simple::Deserialize(storage);

   BOOST_CHECK_EQUAL(in.a, out.a);
   BOOST_CHECK_EQUAL(in.b, out.b);
   BOOST_CHECK_EQUAL(in.c, out.c);
   BOOST_CHECK_EQUAL(in.d, out.d);
   BOOST_CHECK_EQUAL(in.e, out.e);
   BOOST_CHECK_EQUAL(in.f, out.f);
}

BOOST_AUTO_TEST_SUITE_END()